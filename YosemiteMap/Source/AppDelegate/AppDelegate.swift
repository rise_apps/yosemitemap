//
//  AppDelegate.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 11/16/18.
//  Copyright © 2018 Riseapps. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        setupMainModule()
        
        return true
    }
}

// MARK: - Private
extension AppDelegate {
    
    private func setupMainModule() {
        let applicationWindow = UIWindow(frame: UIScreen.main.bounds)
        applicationWindow.backgroundColor = UIColor.white
        
        let navigationVC = UINavigationController()
        applicationWindow.rootViewController = navigationVC
        
        applicationWindow.makeKeyAndVisible()
        
        let mapVC = MapViewController.instantiateFromStoryboard()
        let configurator = MapModuleConfigurator()
        configurator.configure(viewController: mapVC, output: nil)
        
        navigationVC.pushViewController(mapVC, animated: true)
        
        window = applicationWindow
    }
}
