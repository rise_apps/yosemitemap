//
//  UIViewController+Storyboard.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 11/16/18.
//  Copyright © 2018 Riseapps. All rights reserved.
//

import UIKit.UICollectionView

extension UIViewController {
    fileprivate static func storyboard() -> UIStoryboard {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        
        return storyboard
    }
    
    static func instantiateFromStoryboard() -> UIViewController {
        let viewController = storyboard().instantiateViewController(withIdentifier: String(describing: self))
        
        return viewController
    }
}
