//
//  MapInteractor.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 16/11/2018.
//  Copyright © 2018 Riseapps. All rights reserved.
//

import Foundation

class MapInteractor {
    weak var output: MapInteractorOutput!
}

// MARK: - MapInteractorInput
extension MapInteractor: MapInteractorInput {}
