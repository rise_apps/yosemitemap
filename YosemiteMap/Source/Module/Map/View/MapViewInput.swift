//
//  MapViewInput.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 16/11/2018.
//  Copyright © 2018 Riseapps. All rights reserved.
//

protocol MapViewInput: AnyObject {
    func setupInitialState()
}
