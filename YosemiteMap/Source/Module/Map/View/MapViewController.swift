//
//  MapViewController.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 16/11/2018.
//  Copyright © 2018 Riseapps. All rights reserved.
//

import UIKit

class MapViewController: UIViewController {
    var output: MapViewOutput!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        output.viewIsReady()
    }
}

// MARK: - MapViewInput
extension MapViewController: MapViewInput {
    
    func setupInitialState() {
        navigationItem.title = "Yosemite"
    }
}
