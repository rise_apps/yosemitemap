//
//  MapRouterShowProtocol.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 16/11/2018.
//  Copyright © 2018 Riseapps. All rights reserved.
//

import UIKit

protocol MapRouterShowProtocol {

	var viewController: UIViewController! { get }

	func presentMap()
    func showMap()
}

extension MapRouterShowProtocol {
	
	func presentMap() {
        let screenVC = MapViewController.instantiateFromStoryboard()
        let configurator = MapModuleConfigurator()
        configurator.configure(viewController: screenVC, output: nil)
        
        let navVC = UINavigationController(rootViewController: screenVC)
        
        viewController.present(navVC, animated: true, completion: nil)
    }

    func showMap() {
        let screenVC = MapViewController.instantiateFromStoryboard()
        let configurator = MapModuleConfigurator()
        configurator.configure(viewController: screenVC, output: nil)
        
        viewController.navigationController?.pushViewController(screenVC, animated: true)
    }
}
