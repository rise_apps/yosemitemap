//
//  MapRouter.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 16/11/2018.
//  Copyright © 2018 Riseapps. All rights reserved.
//

import UIKit

class MapRouter {
	weak var viewController: UIViewController!
}

// MARK: - MapRouterInput
extension MapRouter: MapRouterInput {}
