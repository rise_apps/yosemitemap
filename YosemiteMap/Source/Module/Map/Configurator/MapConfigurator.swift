//
//  MapConfigurator.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 16/11/2018.
//  Copyright © 2018 Riseapps. All rights reserved.
//

import UIKit

class MapModuleConfigurator {
    
    @discardableResult
    func configure(viewController: UIViewController, output moduleOutput: MapModuleOutput?) -> MapModuleInput? {
        guard let viewController = viewController as? MapViewController else { return nil }

        let router = MapRouter()
        router.viewController = viewController

        let presenter = MapPresenter()
        presenter.view = viewController
        presenter.router = router
        presenter.moduleOutput = moduleOutput

        let moduleInput = presenter

        let interactor = MapInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter

        return moduleInput
    }
}
