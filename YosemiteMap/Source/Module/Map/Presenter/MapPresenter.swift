//
//  MapPresenter.swift
//  YosemiteMap
//
//  Created by Vadym Mitin on 16/11/2018.
//  Copyright © 2018 Riseapps. All rights reserved.
//

import Foundation

class MapPresenter {
	weak var moduleOutput: MapModuleOutput?

	var router: MapRouterInput!

	var interactor: MapInteractorInput!
	
    weak var view: MapViewInput!
}

// MARK: - MapViewOutput
extension MapPresenter: MapViewOutput {
    
    func viewIsReady() {
        view.setupInitialState()
    }
}

// MARK: - MapInteractorOutput
extension MapPresenter: MapInteractorOutput {}

// MARK: - MapModuleInput
extension MapPresenter: MapModuleInput {}
